import psycopg2
from psycopg2.extras import execute_values
from typing import List


connect = psycopg2.connect(
    database="company",
    user="username",
    password="password",
    host="127.0.0.1",
    port="5432",
)
connect.autocommit = False


def insert_salaries(ids: List[int], employee_ids: List[int], amount: List[int]) -> bool:
    cursor = connect.cursor()
    query = "INSERT INTO Salary (id, employee_id, amount) values %s"
    to_insert = []

    for i in range(min(len(ids), len(employee_ids), len(amount))):
        cursor.execute(
            f"SELECT id FROM Salary WHERE employee_id={employee_ids[i]} or id={ids[i]} LIMIT 1;"
        )
        if cursor.fetchone():
            continue
        to_insert.append((ids[i], employee_ids[i], amount[i]))

    try:
        execute_values(cursor, query, to_insert)
        connect.commit()
        return True
    except BaseException as e:
        connect.rollback()
        return False

    cursor.execute("SELECT * from Salary")
    cursor.fetchall()


if __name__ == "__main__":
    insert_salaries([19, 31], [62, 31], [34, 24])
